package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService) {
        this.videoGamesService = videoGamesService;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames() {
        return videoGamesService.ownedVideoGames();
    }



    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public void removeVideoGame(@PathVariable("id") Long id){
        this.videoGamesService.removeGamesFromOurCollection(id);
    }
    

    @GetMapping("/{id}/end")
    public void changesStatus(@PathVariable("id") Long id){
        this.videoGamesService.changeGameStatus(id);
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) {
        return this.videoGamesService.getOneVideoGame(id)
                .map(videoGame -> ResponseEntity.ok(videoGame))
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<VideoGame> addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {
        return this.videoGamesService.addVideoGame(videoGameName.getName())
                .map( v -> ResponseEntity.ok(v))
                .orElse(ResponseEntity.notFound().build());
    }
}
