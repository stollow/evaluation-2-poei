package com.zenika.academy.videogames.domain;

import com.zenika.academy.videogames.GameStatus;

import java.util.List;

public class VideoGame {
    private Long id;
    private String name;
    private List<Genre> genres;
    private GameStatus gameStatus;

    public VideoGame(Long id, String name, List<Genre> genres) {
        this.id = id;
        this.name = name;
        this.genres = List.copyOf(genres);
        this.gameStatus = GameStatus.EN_COURS;
    }

    public VideoGame() {}

    public GameStatus getGameStatus(){
        return this.gameStatus;
    }

    public void setGameStatus(GameStatus status){
        this.gameStatus = status;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Genre> getGenres() {
        return List.copyOf(genres);
    }

    public void setGenres(List<Genre> genres) {
        this.genres = List.copyOf(genres);
    }
}
