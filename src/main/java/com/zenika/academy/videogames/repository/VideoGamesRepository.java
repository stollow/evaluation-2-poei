package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.GameStatus;
import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    @Autowired
    public VideoGamesRepository(){

    }

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        return Optional.ofNullable(this.videoGamesById.get(id));
    }

    public void changeGameStatus(Long id){
        Optional<VideoGame> getVideoGameIfExist = Optional.ofNullable(this.videoGamesById.get(id));
        getVideoGameIfExist.ifPresent(videoGame -> videoGame.setGameStatus(GameStatus.TERMINE));
    }

    public void save(VideoGame v) {
        Optional<VideoGame> getVideoGameIfExist = Optional.ofNullable(this.videoGamesById.get(v.getId()));
        getVideoGameIfExist.ifPresent(videoGame -> v.setGameStatus(videoGame.getGameStatus()));
        this.videoGamesById.put(v.getId(), v);
    }

    public void remove(Long id){
        this.videoGamesById.remove(id);
    }

    @PostConstruct
    public void setup(){
        Genre genre1 = new Genre("MOBA");
        Genre genre2 = new Genre("RPG");
        Genre genre3 = new Genre("Exploration");
        VideoGame videoGame1 = new VideoGame(1L,"league of legend", List.of(genre1));
        VideoGame videoGame2 = new VideoGame(2L,"The legend Of Zelda", List.of(genre2,genre3));
        this.save(videoGame1);
        this.save(videoGame2);
    }
}
