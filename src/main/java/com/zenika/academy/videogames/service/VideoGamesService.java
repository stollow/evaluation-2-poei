package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient client;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient client) {
        this.videoGamesRepository = videoGamesRepository;
        this.client = client;
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public void changeGameStatus(Long id){
        this.videoGamesRepository.changeGameStatus(id);
    }


    public void removeGamesFromOurCollection(Long id){
        this.videoGamesRepository.remove(id);
    }

    public Optional<VideoGame> getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id);
    }

    public Optional<VideoGame> addVideoGame(String name) {
        Optional<VideoGame> newGame = Optional.ofNullable(this.client.getVideoGameFromName(name));

        if(newGame.isPresent()) {
            videoGamesRepository.save(newGame.get());
        }else{
            throw new NoSuchElementException();
        }
        return newGame;
    }
}
