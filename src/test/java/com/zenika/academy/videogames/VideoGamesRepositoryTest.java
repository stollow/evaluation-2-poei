package com.zenika.academy.videogames;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class VideoGamesRepositoryTest {

    VideoGame videoGame1;
    VideoGame videoGame2;
    Genre genre1;
    Genre genre2;
    Genre genre3;
    VideoGamesRepository repository;
    List<VideoGame> videoGames;

    @BeforeEach
    void setup(){
        genre1 = new Genre("MOBA");
        genre2 = new Genre("RPG");
        genre3 = new Genre("Exploration");
        videoGame1 = new VideoGame(3L,"league of legend", List.of(genre1));
        videoGame2 = new VideoGame(4L,"The legend Of Zelda", List.of(genre2,genre3));
        videoGames = List.of(videoGame1,videoGame2);
        repository = new VideoGamesRepository();
        repository.save(videoGame1);
        repository.save(videoGame2);
    }

    @Test
    void getGameFromRepositoryTest(){
        Assertions.assertEquals(repository.get(3L).get(),videoGame1);
    }

    @Test
    void getGameFromRepositoryWithEmptyResponseTest(){
        Assertions.assertEquals(repository.get(5L), Optional.empty());
    }

    @Test
    void getListOfGamesTest(){
        Assertions.assertEquals(repository.getAll(),videoGames);
    }
}
