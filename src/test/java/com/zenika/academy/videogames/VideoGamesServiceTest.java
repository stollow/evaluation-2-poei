package com.zenika.academy.videogames;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

public class VideoGamesServiceTest {
    VideoGamesRepository repository;
    VideoGame videoGame1;
    VideoGame videoGame2;
    Genre genre1;
    Genre genre2;
    Genre genre3;
    List<VideoGame> videoGames;
    VideoGamesService videoGamesService;
    RawgDatabaseClient client;

    @BeforeEach
    void setup(){
        repository = Mockito.mock(VideoGamesRepository.class);
        client = Mockito.mock(RawgDatabaseClient.class);
        genre1 = new Genre("MOBA");
        genre2 = new Genre("RPG");
        genre3 = new Genre("Exploration");
        videoGame1 = new VideoGame(3L,"league of legend", List.of(genre1));
        videoGame2 = new VideoGame(4L,"The Legend of Zelda", List.of(genre2,genre3));
        videoGames = List.of(videoGame1,videoGame2);
        repository.save(videoGame1);
        repository.save(videoGame2);
        videoGamesService = new VideoGamesService(repository,client);
    }

    @Test
    void getOwnedVideoGamesTest(){
        Mockito.when(repository.getAll()).thenReturn(videoGames);
        Assertions.assertEquals(videoGamesService.ownedVideoGames(),videoGames);
    }

    @Test
    void getOneVideoGamesTest(){
        Mockito.when(repository.get(3L)).thenReturn(Optional.of(videoGame1));
        Assertions.assertEquals(videoGamesService.getOneVideoGame(3L),Optional.of(videoGame1));
    }

    @Test
    void addVideoGameToListTest(){
        Mockito.when(client.getVideoGameFromName("The Legend of Zelda")).thenReturn(videoGame2);
        Assertions.assertEquals(videoGamesService.addVideoGame("The Legend of Zelda").get().getName(),videoGame2.getName());
    }
}
